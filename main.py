import csv
import os
import glob

path = './data/tuesday'
csv_files = glob.glob(os.path.join(path, "*.csv"))

for f in csv_files:
  with open(f) as file:
    csvreader = csv.reader(file)
    max = 0.0
    time = ''
    for row in csvreader:
      if (float(row[1]) > float(max)):
        max = row[1]
        time = row[0]
    print(f, max, time)